# Call CMD From PowerShell in GitLab CI

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/gitlab-ci-yml-powershell-tips-tricks-and-hacks/powershell-pipelines-on-gitlab-ci)

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

* **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

* **Publish Date**: 2020-06-09

* **GitLab Version Released On**: 13.0

* **GitLab Edition Required**: 

  * For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

- **Tested On**: 
  - GitLab Docker-Executor Runner (GitLab.com Shared Runner).
  - Windows Shell Runner (GitLab.com Shared Runner).

* **References and Featured In**:
  * Slicing and Dicing with PowerShell on GitLab CI: https://youtu.be/UZvtAYwruFc

## Demonstrates These Design Requirements, Desirements and AntiPatterns

- **GitLab Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Use CMD on a Windows Shell Runner when PowerShell is the default shell (GitLab.com Shared Runner).

## Using PowerShell Under GitLab CI
* GitLab CI Variables are propagated to CMD environment variables 
* Windows Shared runners are shell runners, so they do not have an "image:" tag associated.
* On GitLab.com runners run as the custom "gitlab-runner" account.
* Self-hosted runners can be setup as the system account or a named account: https://docs.gitlab.com/runner/install/windows.html#installation.
* PowerShell will be the default shell on Windows, unless you take specific actions to change it to CMD.

